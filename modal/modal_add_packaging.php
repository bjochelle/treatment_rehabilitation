<form class="form-horizontal" id="form_add" method="post"> 
<div id="myModal" class="modal fade"  data-backdrop="static" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Preparation</h4>
      </div>
      <div class="modal-body">
      <div class="form-body">
          <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Preparation Name</label> 
            <div class="col-sm-9"> 
              <input type="text" name="packaging_desc" class="form-control" required placeholder="Preparation Name" autocomplete="off"> 
            </div> 
          </div> 
      </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success" >Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form> 