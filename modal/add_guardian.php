<form class="form-horizontal" id="form_add" method="post"> 
<div id="myModal" class="modal fade"  data-backdrop="static" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Guardian</h4>
      </div>
      <div class="modal-body">
      <div class="form-body">
            <div class="form-group"> 
              <label for="inputEmail3" class="col-sm-2 control-label">Name:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="name" class="form-control" required placeholder="Name" autocomplete="off"> 
              </div> 
            </div> 
            <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Address:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="address" class="form-control" required placeholder="Address" autocomplete="off"> 
              </div> 
            </div>
            <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Birthdate:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="dob" id="datepicker1" class="form-control" required placeholder="Date of Birth" autocomplete="off"> 
              </div> 
            </div>
            <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Contact Number:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="contact_number"  class="form-control" required placeholder="Contact Number" autocomplete="off"> 
              </div> 
            </div>
             <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Email:</label> 
              <div class="col-sm-9"> 
                <input type="email" name="email" class="form-control" required placeholder="Email" autocomplete="off">
              </div> 
            </div> 
          </div> 
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success" >Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form> 