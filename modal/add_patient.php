<form class="form-horizontal" id="form_add" method="post"> 
<div id="myModal" class="modal fade"  data-backdrop="static" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Patient</h4>
      </div>
      <div class="modal-body">
      <div class="form-body">
          <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Guardian Name</label> 
            <div class="col-sm-9"> 
            <select name="guardian_id" id="selector1" class="form-control1" required=" ">
                <option>--Please select Guardian--</option>
                <?php 
                include "core/config.php";
                $fetch_product =mysql_query("SELECT * FROM tbl_user where status ='G'");
                while($row=mysql_fetch_array($fetch_product)){
                  echo "<option value=".$row['user_id'].">".$row['name']."</option>";
                }
                ?>
            </select>

            </div> 
          </div> 
           <div class="form-group"> 
              <label for="inputEmail3" class="col-sm-2 control-label">First Name:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="patient_name" class="form-control" required placeholder="FirstName" autocomplete="off"> 
              </div> 
            </div> 
            <div class="form-group"> 
              <label for="inputEmail3" class="col-sm-2 control-label">Middle Name:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="patient_mname" class="form-control" required placeholder="Middle Name" autocomplete="off"> 
              </div> 
            </div> 
            <div class="form-group"> 
              <label for="inputEmail3" class="col-sm-2 control-label">Last Name:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="patient_lname" class="form-control" required placeholder="Last Name" autocomplete="off"> 
              </div> 
            </div> 
            <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Address:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="patient_address" class="form-control" required placeholder="Address" autocomplete="off"> 
              </div> 
            </div>
            <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Birthdate:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="patient_dob" id="datepicker1" class="form-control" required placeholder="Date of Birth" autocomplete="off"> 
              </div> 
            </div>
             <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Remarks:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="patient_remarks" class="form-control" required placeholder="Remarks" autocomplete="off" > 
              </div> 
            </div>
      </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success" >Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form> 