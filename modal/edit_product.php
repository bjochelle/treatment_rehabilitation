<form class="form-horizontal" id="form_edit" method="post"> 
<div id="editModal" class="modal fade"  data-backdrop="static" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Product</h4>
      </div>
      <div class="modal-body">
      <div class="form-body">
              <input type="hidden" name="product_id" class="form-control" id="hidden_id" required placeholder="Product" autocomplete="off"> 

          <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Brand Name</label> 
            <div class="col-sm-9"> 
              <input type="text" name="prod_name" class="form-control" id="prod_name" required placeholder="Brand Name" autocomplete="off"> 
            </div> 
          </div> 
          <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Generic Name</label> 
            <div class="col-sm-9"> 
              <input type="text" name="generic_name" class="form-control" id="generic_name" required placeholder="generic Name" autocomplete="off"> 
            </div> 
          </div> 
          <div class="form-group"> 
            <label for="inputPassword3" class="col-sm-2 control-label">Description</label> 
            <div class="col-sm-9"> 
              <input type="text" name="prod_desc" class="form-control" id="prod_desc" required placeholder="Description" autocomplete="off"> 
            </div> 
          </div>
      </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success" >Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form> 