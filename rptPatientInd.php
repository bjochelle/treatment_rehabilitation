<style>
	button.btn.btn-default{
		width: 100%;
		background: white;
		border: 1px solid #c1c1c1;
		color: #c1c1c1;
		padding: 5px;
		text-align: left;
	}

	.btn-group, .btn-group-vertical{
		width: 100%;
	}
</style>
<div class="banner">
    <h2>
        <a href="home.php?page=dashboard">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Report</span>
        <i class="fa fa-angle-right"></i>
        <span>Patient's Individual Report</span>

    </h2>
</div>

<div class="blank">
    <div class="blank-page">
    <div class="table-heading">
		<h2>Patient's Individual Report</h2>
	</div>
	<div class="agile-tables">
		<div class="w3l-table-info">
			<div class="form-group col-sm-12"> 
				<div class="col-sm-2"> 
	              <label for="inputPassword3" class="control-label">Patient Name :</label> 
	               
	              </div> 
	              <div class="col-sm-6"> 
	                <select name="patient_id" id="select_search_all"  class="form-control" required="">
		                <?php 
		                include "core/config.php";
		                $fetch_product =mysql_query("SELECT * FROM tbl_patient  ORDER BY `tbl_patient`.`patient_name` ASC");
		                while($row=mysql_fetch_array($fetch_product)){
		                  echo "<option value=".$row['patient_id'].">".$row['patient_name']." ".$row['patient_mname']."  ".$row['patient_lname']."</option>";
		                }
		                ?>
		            </select>
	               </div> 
	              <div class="col-sm-2"> 
	    			  <button type="button" class="btn btn-primary" id="btn_gen" onclick="gen()"><span class='fa fa-refresh'></span> Generate  Report</button>

	              </div> 
            </div>

       
           
		</div>
		<br>
		<div class="row" id="report_data">
           </div>
	</div>
    </div>
</div>

<script type="text/javascript">
	
function gen(){
	

	var id = $("#select_search_all").val();
	if(id == "" ){
		alert("Please fill in the form");
	}else{
		$("#btn_gen").prop("disabled",true);
		$("#btn_gen").html("<span class='fa fa-spin fa-spinner'></span> Loading");
		$.ajax({
			type:"POST",
			url:"ajax/rpt_patientInd.php",
			data:{
				id:id
			},success:function(data){

				$("#report_data").html(data);
				$("#btn_gen").prop("disabled",false);
				$("#btn_gen").html("<span class='fa fa-refresh'></span> Generate Report");
			}
		});
	}
}
</script>