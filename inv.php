<div class="banner">
    <h2>
        <a href="home.php?page=dashboard">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Report</span>
        <i class="fa fa-angle-right"></i>
        <span>Inventory Report</span>

    </h2>
</div>

<div class="blank">
    <div class="blank-page">
    <div class="table-heading">
		<h2>Inventory Report</h2>
	</div>
	<div class="agile-tables">
		<div class="w3l-table-info">
			<div class="form-group col-sm-12"> 
				<div class="col-sm-2"> 
	              <label for="inputPassword3" class="control-label">Date :</label> 
	              </div> 
	              <div class="col-sm-4"> 
	                <input type="text" name="dob" autocomplete='off' id="datepicker1" class="form-control" required placeholder="Date"> 
	              </div> 
	              <div class="col-sm-4"> 
	    			  <button type="button" class="btn btn-primary" id="btn_gen" onclick="gen()"><span class='fa fa-refresh'></span> Generate  Report</button>

	              </div> 
            </div>
           
		</div>
		<br>
		<div class="row" id="report_data">

        </div>
	</div>
    </div>
</div>
<script type="text/javascript">
	
function gen(){
	var sd = $("#datepicker1").val();
	if(sd == ""){
		alert("Please fill in the form");
	}else{
		$("#btn_gen").prop("disabled",true);
		$("#btn_gen").html("<span class='fa fa-spin fa-spinner'></span> Loading");
		$.ajax({
			type:"POST",
			url:"ajax/rpt_inventory.php",
			data:{
				sd:sd
			},success:function(data){

				$("#report_data").html(data);
				$("#btn_gen").prop("disabled",false);
				$("#btn_gen").html("<span class='fa fa-refresh'></span> Generate Report");
			}
		});
	}
}
</script>