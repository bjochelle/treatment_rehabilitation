<div class="banner">
    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Patient</span>
    </h2>
</div>

<div class="blank">
    <div class="blank-page">
    <div class="table-heading">
		<h2>Patient List</h2>
	</div>
	<div class="agile-tables">
		<div class="w3l-table-info">
			<div class="form-group pull-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" ><span class="fa fa-plus-circle"> </span> Add Patient</button>
   <!--      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalExisting" ><span class="fa fa-plus-circle"> </span> Add Existing Patient</button> -->
			</div>
			<table id="table">
			<thead>
				<tr>
				<th>#</th>
				<th>Patient Code</th>
				<th>Patient Name</th>
				<th>Guardian</th>
        <th>Remarks</th>
        <th>Date Added</th>
        <th>Date Discharged</th>
        <th>Status</th>
				<th>Action</th>
				</tr>
			</thead>
			<tbody>
			
			</tbody>
			</table>
			<?php require "modal/add_patient.php";?>
			<?php require "modal/edit_patient.php";?>
      <?php require "modal/add_exist_patient.php";?>


		</div>
	</div>
    </div>
</div>

<script src="js/jquery_updated.min.js"></script>
<script>
$(document).ready(function(){
  	getData();
$("#form_add").submit(function(e){
  e.preventDefault();
$.ajax({
    url:"ajax/add_patient.php",
    method:"POST",
    data:$(this).serialize(),
    success:function(data){
    if(data==1){
      notify('success','Successfully Added.','glyphicon glyphicon-ok');
      getData();
      $("#myModal").modal('hide');
      $("#form_add")[0].reset();
    }else if(data==2){
      notify('warning','Duplicate Entry.','glyphicon glyphicon-exclamation-sign');

    }else{
       notify('error','Something went wrong.','glyphicon glyphicon-exclamation-sign');

    }
    }
  })
   });
$("#form_edit").submit(function(e){
  e.preventDefault();
$.ajax({
    url:"ajax/update_patient.php",
    method:"POST",
    data:$(this).serialize(),
    success:function(data){
    $("#editModal").modal('hide');
    if(data==1){
      notify('white','Successfully Updated.');
      getData();
      $("#form_edit")[0].reset();
    }else if(data==2){
      notify('warning','Duplicate Entry.','glyphicon glyphicon-exclamation-sign');

    }else{
     notify('Red','Error. Please try again later.');

    }
    }
  })
   });



});


function editDetails(id){
	$("#editModal").modal("show");
	 var parameter = "tbl_patient where patient_id ="+id;
    $.ajax({
        url:"ajax/getDetails.php",
        type:"POST",
        data:{
            parameter:parameter
        },success:function(data){

        	var o = JSON.parse(data);

          $("#guardian").val(o.guardian_id);
          $("#name").val(o.patient_name);
          $("#patient_mname").val(o.patient_mname);
          $("#patient_lname").val(o.patient_lname);
          $("#patient_address").val(o.patient_address);
          $("#datepicker2").val(o.patient_dob);
          $("#remarks").val(o.patient_remarks);
          $("#hidden_id").val(id)

        }
    });
}

function update_status(id,status){
    if(status == 1){
      var value= 'Released'
    }else if(status == 0){
      var value= 'Admitted'
    }else{
      var value= 'Extended'
    }

    $.ajax({
        url:"ajax/update_patient_status.php",
        type:"POST",
        data:{
            id:id,
            value:value
        },success:function(data){
          if(data==1){
              notify('success','Successfully Updated.','glyphicon glyphicon-ok');
              getData();
          }else{
             notify('error','Unable to update');

          }

        }
    });
}



function getData(){
  var table = $('#table').DataTable();
  table.destroy();
  $("#table").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/dt_patient.php",
      "dataSrc":"data"
    },
    "columns":[
       {
        "data":"count"
      },
      {
        "data":"code"
      },
      {
        "data":"name"
      },
      {
        "data":"guardian"
      },
      {
        "data":"remarks"
      },
      {
        "data":"date_added"
      },
      {
        "data":"date_discharge"
      },
      {
        "data":"status"
      },
      {
        "mRender": function(data,type,row){
          if(row.status == 'Released'){
            var btn = "<button class='btn btn-warning btn-sm' data-toggle='tooltip' title='Admit' value='"+row.id+ "' id='"+row.id+"' onclick='update_status("+row.id+",2)'><span class='fa fa-wheelchair'></span> </button>";
          }else{
            var btn = "<button class='btn btn-success btn-sm' data-toggle='tooltip' title='Releasing' value='"+row.id+ "' id='"+row.id+"' onclick='update_status("+row.id+",1)'><span class='fa fa-thumbs-up'></span> </button>";

          }
           return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Edit Record' value='"+ row.id+ "' id='"+row.id+"' onclick='editDetails("+row.id+")'><span class='fa fa-pencil'></span> </button>"+btn+"<button class='btn btn-info btn-sm' data-toggle='tooltip' title='Extend' value='"+ row.id+ "' id='"+row.id+"' onclick='update_status("+row.id+",2)'><span class='fa fa-expand'></span> </button></center>";
          }
        
      }
    ]
  });
}
</script>