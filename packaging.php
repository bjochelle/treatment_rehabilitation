<div class="banner">
    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Preparation</span>
    </h2>
</div>

<div class="blank">
    <div class="blank-page">
    <div class="table-heading">
		<h2>Preparation List</h2>
	</div>
	<div class="agile-tables">
		<div class="w3l-table-info">
			<div class="form-group pull-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" ><span class="fa fa-plus-circle"> </span> Add Preparation</button>
	
			</div>
			<table id="table">
			<thead>
				<tr>
				<th>#</th>
				<th>Preparation</th>
        <th></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
			<?php require "modal/modal_add_packaging.php";?>
			<?php require "modal/modal_edit_packaging.php";?>
		</div>
	</div>
    </div>
</div>
<script src="js/jquery_updated.min.js"></script>

<script>
$(document).ready(function(){
  	getData();

  $("#form_add").submit(function(e){
  e.preventDefault();
  $.ajax({
    url:"ajax/add_packaging.php",
    method:"POST",
    data:$(this).serialize(),
    success:function(data){
    if(data==1){
       notify('success','Successfully Added.','glyphicon glyphicon-ok');
      getData();
      $("#myModal").modal('hide');
      $("#form_add")[0].reset();
    }else if(data==2){
     notify('warning','Duplicate Entry.','glyphicon glyphicon-exclamation-sign');
    }else{
      notify('error','Something went wrong.','glyphicon glyphicon-exclamation-sign')

    }
    }
  })
   });


  $("#form_edit").submit(function(e){
  e.preventDefault();
  $.ajax({
    url:"ajax/update_packaging.php",
    method:"POST",
    data:$(this).serialize(),
    success:function(data){
    if(data==1){
       notify('success','Successfully Updated.','glyphicon glyphicon-ok');
      getData();
      $("#editModal").modal('hide');
    }else if(data==2){
       notify('warning','Duplicate Entry.','glyphicon glyphicon-exclamation-sign');

    }else{
       notify('error','Something went wrong.','glyphicon glyphicon-exclamation-sign');

    }
    }
  })
   });

});
function editDetails(id){
	$("#editModal").modal("show");
  var parameter = "tbl_packaging where packaging_id ="+id;
    $.ajax({
        url:"ajax/getDetails.php",
        type:"POST",
        data:{
            parameter:parameter
        },success:function(data){
        var o = JSON.parse(data);

          $("#prod_name").val(o.packaging_desc);
          $("#hidden_id").val(id)

        }
    });
}
function getData(){
  var table = $('#table').DataTable();
  table.destroy();
  $("#table").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/dt_packaging.php",
      "dataSrc":"data"
    },
    "columns":[
       {
        "data":"count"
      },
      {
        "data":"name"
      },
       {
        "mRender": function(data,type,row){
           return "<center><button class='btn btn-success btn-sm' data-toggle='tooltip' title='Edit Record' value='"+ row.id+ "' id='"+row.id+"' onclick='editDetails("+row.id+")'><span class='fa fa-pencil'></span> Edit </button></center>";
          
        }
      }

    ]
  });
}


</script>