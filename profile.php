<?php 
include 'core/config.php';
$user_id=$_SESSION['id'];

$r= mysql_fetch_array(mysql_query("SELECT * from tbl_user where user_id='$id'"));
?>
<div class="progressbar-heading grids-heading">
	<h2>User Profile</h2>
</div>

<div class="blank">
    <div class="blank-page">
       <div class="panel panel-widget forms-panel">
			<div class="forms">
				<div class=" form-grids form-grids-right">
					<div class="widget-shadow " data-example-id="basic-forms"> 
						<div class="form-body">
							<form class="form-horizontal" action="#" method="post" id="form_edit"> 
								<input type="hidden" value="<?php echo $r['user_id'];?>" name="user_id" id="field-1" required="true" class="form-control">
								<div class="form-group"> 
									<label for="inputEmail3" class="col-sm-2 control-label">Name</label> 
									<div class="col-sm-9"> 
										<input type="text" name="name" value="<?php echo $r['name'];?>" class="form-control" id="inputEmail3" placeholder="Name" autocomplete="off"> 
									</div> 
								</div> 
								<div class="form-group"> 
									<label for="inputPassword3" class="col-sm-2 control-label">Address</label> 
									<div class="col-sm-9"> 
										<input type="text" name="address" value="<?php echo $r['address'];?>" class="form-control" id="inputPassword3" placeholder="Address" autocomplete="off"> 
									</div> 
								</div>  
								<div class="form-group"> 
									<label for="inputPassword3" class="col-sm-2 control-label">Email</label> 
									<div class="col-sm-9"> 
										<input type="email" name="email" value="<?php echo $r['user_id'];?>" class="form-control" id="inputPassword3" placeholder="Email" autocomplete="off"> 
									</div> 
								</div> 
								<div class="form-group"> 
									<label for="inputPassword3" class="col-sm-2 control-label">Username</label> 
									<div class="col-sm-9"> 
										<input type="text" name="un" value="<?php echo $r['un'];?>" class="form-control" id="inputPassword3" placeholder="Username" autocomplete="off"> 
									</div> 
								</div>
								<div class="col-sm-offset-2"> 
									<button type="submit" class="btn btn-default w3ls-button">Update</button> 
								</div> 
							</form> 
						</div>
					</div>
				</div>
			</div>	
		</div>
    </div>
</div>

<script src="js/jquery_updated.min.js"></script>

<script>
$(document).ready(function(){
  $("#form_edit").submit(function(e){
  e.preventDefault();
  $.ajax({
    url:"ajax/update_profile_user.php",
    method:"POST",
    data:$(this).serialize(),
    success:function(data){
    if(data==1){
       notify('success','Successfully Updated.','glyphicon glyphicon-ok');
      getData();
      $("#editModal").modal('hide');
    }else if(data==2){
       notify('warning','Duplicate Entry.','glyphicon glyphicon-exclamation-sign');

    }else{
       notify('error','Something went wrong.','glyphicon glyphicon-exclamation-sign');

    }
    }
  })
   });

});


</script>