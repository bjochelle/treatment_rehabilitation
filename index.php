<!DOCTYPE html>
<head>
<title>Treatment and Rehabilitation</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />

 <link rel="icon" type="image/ico" href="images/logo.png" />
<!-- bootstrap-css -->
<link rel="stylesheet" href="css/bootstrap.css">
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<link href='css/font.css' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
<link rel="stylesheet" href="css/font.css" type="text/css"/>
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<style>
	.register{
		background-color: #03a9f4;
   	 	border: solid 2px #03a9f4;
	}
</style>
</head>
<body class="signup-body">

  <div class="col-md-6" style="top: 100px;">
     <center><img src="images/logo.png" width="70%">
   <h1 style="    color: #035f74;
    text-shadow: 1px 1px 3px #f7f7f7;">  Treatment and Rehabilitation </h1></center>
  </div>

   <div class="col-md-6" style="top: 100px;">
      <div class="agile-signup">  
      
      <div class="content2"style="width: 70% !important;">
        <div class="grids-heading gallery-heading signup-heading" style='background:#03a9f4;'>
          <h2 style='color:white;'>Login</h2>
        </div>
        <form id="sign_in" method="post">
          <input required id="un" type="text" name="un" placeholder="Username">
          <input required id="pw" type="password" name="pw" placeholder="Password" >
          <input type="submit" class="register" value="Login" id="submit">
        </form>
        
        <span id="notif"> </span>
      </div>
      
      
      
    </div>
  </div>
	
</body>
</html>

<script type="text/javascript">
  $("#sign_in").submit(function(e){
  	 e.preventDefault();
    var un = $("#un").val();
    var pw = $("#pw").val();
 
     $("#notif").removeClass("shake");
     $("input[type='submit']").prop("disabled",true);
     $("input[type='submit']").val("Please Wait");

    $.ajax({
    url:"ajax/login_user.php",
    method:"POST",
     data:$("#sign_in").serialize(),
        success: function(data){

          if(data == 1){
          window.location = 'home.php?page=dashboard';
        }else{
         $("#notif").html("<span style='color:red'>Incorrect Username or Password</span>");
         $("#notif").addClass("shake");
        }
     $("input[type='submit']").val("Login");

      }
    });
  });
</script>
