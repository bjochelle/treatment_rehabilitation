<div class="banner">
    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Product</span>
    </h2>
</div>

<div class="blank">
    <div class="blank-page">
    <div class="table-heading">
		<h2>Product List</h2>
	</div>
	<div class="agile-tables">
		<div class="w3l-table-info">
			<div class="form-group pull-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" ><span class="fa fa-plus-circle"> </span> Add Product</button>
	
			</div>
			<table id="table">
			<thead>
				<tr>
				<th>#</th>
				<th>Brand Name</th>
        <th>Generic Name</th>
				<th>Description</th>
        <th></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
			<?php require "modal/add_product.php";?>
			<?php require "modal/edit_product.php";?>
		</div>
	</div>
    </div>
</div>
<script src="js/jquery_updated.min.js"></script>

<script>
$(document).ready(function(){
  	getData();

  $("#form_add").submit(function(e){
  e.preventDefault();
  $.ajax({
    url:"ajax/add_product.php",
    method:"POST",
    data:$(this).serialize(),
    success:function(data){
    if(data==1){
      notify('success','Successfully Added.','glyphicon glyphicon-ok');
      getData();
      $("#myModal").modal('hide');
      $("#form_add")[0].reset();
    }else if(data==2){
      notify('warning','Duplicate Entry.','glyphicon glyphicon-exclamation-sign');

    }else{
       notify('error','Something went wrong.','glyphicon glyphicon-exclamation-sign');

    }
    }
  })
   });


  $("#form_edit").submit(function(e){
  e.preventDefault();
  $.ajax({
    url:"ajax/update_product.php",
    method:"POST",
    data:$(this).serialize(),
    success:function(data){
    if(data==1){
       notify('success','Successfully Updated.','glyphicon glyphicon-ok');
      getData();
      $("#editModal").modal('hide');
    }else if(data==2){
       notify('warning','Duplicate Entry.','glyphicon glyphicon-exclamation-sign');

    }else{
       notify('error','Something went wrong.','glyphicon glyphicon-exclamation-sign');

    }
    }
  })
   });

});
function editDetails(id){
	$("#editModal").modal("show");
  var parameter = "tbl_product where product_id ="+id;
    $.ajax({
        url:"ajax/getDetails.php",
        type:"POST",
        data:{
            parameter:parameter
        },success:function(data){
        var o = JSON.parse(data);

          $("#prod_name").val(o.product_name);
          $("#generic_name").val(o.generic_name);
          $("#prod_desc").val(o.product_desc);
          $("#hidden_id").val(id)

        }
    });
}
function getData(){
  var table = $('#table').DataTable();
  table.destroy();
  $("#table").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/dt_prod.php",
      "dataSrc":"data"
    },
    "columns":[
       {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "data":"generic_name"
      },
      {
        "data":"description"
      },
       {
        "mRender": function(data,type,row){
           return "<center><button class='btn btn-success btn-sm' data-toggle='tooltip' title='Edit Record' value='"+ row.id+ "' id='"+row.id+"' onclick='editDetails("+row.id+")'><span class='fa fa-pencil'></span> Edit </button></center>";
          
        }
      }

    ]
  });
}


</script>