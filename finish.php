<style>
	button.btn.btn-default{
		width: 100%;
		background: white;
		border: 1px solid #c1c1c1;
		color: #c1c1c1;
		padding: 5px;
		text-align: left;
	}

	.btn-group, .btn-group-vertical{
		width: 100%;
	}
</style>
<div class="banner">
    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Finish Task</span>
    </h2>
</div>

<div class="blank">
    <div class="blank-page">
    <div class="table-heading">
		<h2>Finish Task</h2>
	</div>
	<div class="agile-tables">
		<div class="w3l-table-info">
			<table id="table">
			<thead>
				<tr>
				<th>#</th>
				<th>Patient Name</th>
				<th>Product</th>
				<th>Dosage</th>
				<th>Schedule</th>
        <th>Assigned Nurse</th>
				<th>Remarks</th>

				</tr>
			</thead>
			<tbody>
				
			</tbody>
			</table>
			<?php require "modal/add_medvacc.php";?>
			<?php require "modal/edit_medvacc.php";?>

		</div>
	</div>
    </div>
</div>

<script src="js/jquery_updated.min.js"></script>
<script>
$(document).ready(function(){
  	getData();
});
function getData(){
  var table = $('#table').DataTable();
  table.destroy();
  $("#table").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/dt_medvacc_finish.php",
      "dataSrc":"data"
    },
    "columns":[
       {
        "data":"count"
      },
      {
        "data":"patient_name"
      },
      {
        "data":"product"
      },
      {
        "data":"dosage"
      },
      {
        "data":"sched_date"
      },
      {
        "data":"nurse"
      },
      {
        "data":"remarks"
      }
    ]
  });
}
</script>