-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2021 at 01:14 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u764488932_treatment`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_med_vacc_sched`
--

CREATE TABLE `tbl_med_vacc_sched` (
  `med_vacc_sched_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `packaging_id` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `dosage` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sched_date` date NOT NULL,
  `sched_time` time NOT NULL,
  `remarks` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `read_status` int(1) NOT NULL,
  `date_applied` datetime NOT NULL,
  `assign_nurse_id` int(11) NOT NULL,
  `nurse_note` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_med_vacc_sched`
--

INSERT INTO `tbl_med_vacc_sched` (`med_vacc_sched_id`, `patient_id`, `product_id`, `packaging_id`, `quantity`, `dosage`, `sched_date`, `sched_time`, `remarks`, `status`, `read_status`, `date_applied`, `assign_nurse_id`, `nurse_note`) VALUES
(1, 1, 2, 1, '1.00', '1.000', '2020-11-03', '11:30:00', 'urgent', 1, 1, '2020-11-03 11:53:28', 18, ''),
(2, 3, 5, 1, '1.00', '2.000', '2020-11-03', '14:40:00', 'urgent', 1, 0, '2020-11-03 14:40:46', 18, ''),
(3, 3, 5, 1, '1.00', '2.000', '2020-11-03', '15:05:00', '2 tablets', 0, -1, '0000-00-00 00:00:00', 18, ''),
(4, 1, 1, 1, '1.00', '3.000', '2020-11-03', '15:40:00', 'urgent', 1, 1, '2020-11-03 15:38:14', 18, ''),
(5, 1, 6, 1, '1.00', '10ml', '2020-11-28', '21:57:00', 'test', 0, 0, '0000-00-00 00:00:00', 21, ''),
(6, 2, 6, 1, '1.00', '10ml', '2020-11-28', '21:57:00', 'test', 0, 0, '0000-00-00 00:00:00', 21, ''),
(7, 3, 6, 1, '1.00', '10ml', '2020-11-28', '21:57:00', 'test', 0, 0, '0000-00-00 00:00:00', 21, ''),
(8, 4, 3, 1, '1.00', '1ml', '2020-12-10', '15:30:00', 'urgent', 1, 1, '2020-12-10 15:27:44', 18, ''),
(9, 4, 3, 1, '1.00', '2ml', '2021-01-20', '15:00:00', 'urgent', 1, 0, '2021-01-20 14:31:37', 18, ''),
(10, 4, 3, 1, '1.00', '1ml', '2021-01-24', '18:30:00', 'test', 1, 0, '2021-01-24 18:31:54', 18, 'Getting better'),
(11, 1, 1, 1, '1.00', '5ml', '2021-02-02', '15:33:00', 'Urgent', 0, 0, '0000-00-00 00:00:00', 18, ''),
(12, 4, 1, 1, '1.00', '5ml', '2021-02-02', '13:36:00', 'Urgent', 0, 0, '0000-00-00 00:00:00', 18, ''),
(13, 3, 5, 1, '1.00', '5ml', '2021-02-01', '16:40:00', 'Urgent', 1, 0, '2021-02-01 15:44:48', 18, 'Doing well'),
(14, 4, 3, 1, '47.00', '2ml', '2021-02-10', '15:00:00', 'now', 1, 0, '2021-02-10 14:49:45', 18, 'Not feeling well'),
(15, 1, 1, 4, '1.00', 'drops', '2021-02-17', '08:40:00', 'urgent', 0, 0, '0000-00-00 00:00:00', 18, ''),
(16, 2, 1, 4, '1.00', 'drops', '2021-02-17', '08:40:00', 'urgent', 0, 0, '0000-00-00 00:00:00', 18, ''),
(17, 3, 1, 4, '1.00', 'drops', '2021-02-17', '08:40:00', 'urgent', 0, 0, '0000-00-00 00:00:00', 18, ''),
(18, 4, 1, 4, '1.00', 'drops', '2021-02-17', '08:40:00', 'urgent', 0, 0, '0000-00-00 00:00:00', 18, ''),
(19, 1, 1, 2, '2.00', '2pcs', '2021-02-18', '08:30:00', '', 0, 0, '0000-00-00 00:00:00', 18, ''),
(20, 2, 1, 2, '2.00', '2pcs', '2021-02-18', '08:30:00', '', 0, 0, '0000-00-00 00:00:00', 18, ''),
(21, 3, 1, 2, '2.00', '2.00pcs', '2021-02-18', '08:30:00', '', 0, 0, '0000-00-00 00:00:00', 18, ''),
(22, 4, 1, 2, '2.00', '2.00pcs', '2021-02-18', '08:30:00', '', 0, 0, '0000-00-00 00:00:00', 18, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notif`
--

CREATE TABLE `tbl_notif` (
  `notif_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `date_added` datetime NOT NULL,
  `view_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_packaging`
--

CREATE TABLE `tbl_packaging` (
  `packaging_id` int(11) NOT NULL,
  `packaging_desc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_packaging`
--

INSERT INTO `tbl_packaging` (`packaging_id`, `packaging_desc`) VALUES
(1, 'ml'),
(2, 'pcs'),
(3, 'mg'),
(4, 'drops');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_patient`
--

CREATE TABLE `tbl_patient` (
  `patient_id` int(11) NOT NULL,
  `guardian_id` int(11) NOT NULL,
  `patient_code` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_name` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_mname` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_lname` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_dob` date NOT NULL,
  `patient_address` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_remarks` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_assign_nurse_id` int(11) NOT NULL,
  `patient_added_user_id` int(11) NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL,
  `date_discharge` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_patient`
--

INSERT INTO `tbl_patient` (`patient_id`, `guardian_id`, `patient_code`, `patient_name`, `patient_mname`, `patient_lname`, `patient_dob`, `patient_address`, `patient_remarks`, `patient_assign_nurse_id`, `patient_added_user_id`, `status`, `date_added`, `date_discharge`) VALUES
(1, 17, 'BDS19990420', 'Billy ', 'D', 'Susano', '1999-04-20', 'Brgy Handumanan', 'Drug Addict', 0, 0, 'Released', '0000-00-00 00:00:00', '2020-12-10 15:14:03'),
(2, 19, 'JMP19980828', 'Jessa ', 'M', 'Pareneas', '1998-08-28', 'Lacson St. Bacolod City', 'Drug addict', 0, 0, 'Released', '0000-00-00 00:00:00', '2021-02-01 15:45:13'),
(3, 20, 'JD20000307', 'John Doe', '', '', '2000-03-07', 'Zone 12, brgy. Handumanan', 'drug addict', 0, 0, 'Released', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 17, 'MAV1994/11/5', 'Michael', 'A', 'Villeta', '1994-11-05', 'Brgy. Handumanan BC', 'Cocaine Addict', 0, 0, 'Released', '2020-12-10 15:24:10', '2021-02-01 15:45:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `generic_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_desc` varchar(2255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `product_name`, `generic_name`, `product_desc`) VALUES
(1, 'Naltrexone', '(Vivitrol)', 'Tablet'),
(2, 'Buprenorphine', '(Probuphine Suboxone)', 'Syrup'),
(3, 'Topiramate', '(Topamax)', 'Tablet'),
(4, 'Baclofen', '(Lioresal)', 'Tablet'),
(5, 'Biogesic', '(Paracetamol)', 'Tablet'),
(6, 'Synthroid', 'levothyroxine', 'Tablet');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supply`
--

CREATE TABLE `tbl_supply` (
  `supply_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `packaging_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_supply`
--

INSERT INTO `tbl_supply` (`supply_id`, `product_id`, `quantity`, `date_added`, `user_id`, `packaging_id`) VALUES
(1, 2, 70, '2020-11-01', 1, 1),
(2, 1, 50, '2020-11-03', 1, 1),
(3, 5, 50, '2020-11-03', 1, 1),
(4, 3, 50, '2020-12-10', 1, 1),
(5, 5, 50, '2021-02-01', 1, 1),
(6, 1, 1, '2021-02-16', 1, 1),
(7, 3, 111, '2021-02-16', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `un` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pw` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` date NOT NULL,
  `name` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `address` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `un`, `pw`, `date_added`, `name`, `dob`, `address`, `email`, `contact_number`, `status`) VALUES
(1, 'admin', 'admin', '2020-03-26', 'Administrator', '1996-12-07', 'villlamonte bacolod city', 'adfaasd@yahoo.com', '', 'A'),
(17, 'GM19950307', '12345', '2020-11-01', 'Gerrimei Mantalaba', '1995-03-07', 'Phase 6-A Brgy.Handumanan B.C', 'Gergerking@ymail.com', '09089719370', 'G'),
(18, 'NC20201101', '12345', '2020-11-01', 'Nurse Chen', '1989-08-07', 'Brgy.22', 'Chenchen@gmail.com', '', 'N'),
(19, 'TS20201103', '12345', '2020-11-03', 'Trixie Sison', '1997-08-03', 'Lacson St. Bacolod City', 'trixiesison@yahoo.com', '09483652933', 'G'),
(20, 'RL19990510', '12345', '2020-11-03', 'Raymund Llereza', '1999-05-10', 'Zone 12, brgy. Handumanan', 'llereza@gmail.com', '09515666718', 'G'),
(21, 'CB19980201', '12345', '2020-11-03', 'Christian Barelia', '1998-02-01', 'Zone 12, Brgy. Handumanan', 'nino@gmail.com', '', 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_med_vacc_sched`
--
ALTER TABLE `tbl_med_vacc_sched`
  ADD PRIMARY KEY (`med_vacc_sched_id`);

--
-- Indexes for table `tbl_notif`
--
ALTER TABLE `tbl_notif`
  ADD PRIMARY KEY (`notif_id`);

--
-- Indexes for table `tbl_packaging`
--
ALTER TABLE `tbl_packaging`
  ADD PRIMARY KEY (`packaging_id`);

--
-- Indexes for table `tbl_patient`
--
ALTER TABLE `tbl_patient`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_supply`
--
ALTER TABLE `tbl_supply`
  ADD PRIMARY KEY (`supply_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_med_vacc_sched`
--
ALTER TABLE `tbl_med_vacc_sched`
  MODIFY `med_vacc_sched_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_notif`
--
ALTER TABLE `tbl_notif`
  MODIFY `notif_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_packaging`
--
ALTER TABLE `tbl_packaging`
  MODIFY `packaging_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_patient`
--
ALTER TABLE `tbl_patient`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_supply`
--
ALTER TABLE `tbl_supply`
  MODIFY `supply_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
