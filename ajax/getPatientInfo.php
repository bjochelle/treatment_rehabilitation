<?php 
include '../function.php';
$name=$_GET['name'];

$f=mysql_query("SELECT * FROM tbl_patient where patient_name like '$name'");

$r = mysql_fetch_array($f);

$id= $r['patient_id'];
?>
<!DOCTYPE html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- bootstrap-css -->
<link rel="stylesheet" href="../css/bootstrap.css">
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="../css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<link rel="stylesheet" href="../css/font.css" type="text/css"/>
<link href="../css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<script src="../js/jquery_updated.min.js"></script>
<script src="../js/modernizr.js"></script>
<script src="../js/jquery.cookie.js"></script>
<script src="../js/screenfull.js"></script>
<link rel="stylesheet" type="text/css" href="../css/table-style.css" />
<link rel="stylesheet" type="text/css" href="../css/basictable.css" />
		</head>
<body class="dashboard-page">


	<section class="wrapper scrollable">
		
		<div>
			<div class="agile-grids">	
				<!-- input-forms -->
					<div class="progressbar-heading grids-heading">
						<h2>Patient Details</h2>
					</div>

				<hr style="border-top: 2px solid #020202;">
				<div class="widget-shadow " data-example-id="basic-forms"> 
					
					<div>
						
						<div class="form-group"> 
							<label for="inputPassword3" class="col-sm-2 control-label"><h4>Patient Code  </h4></label> 
							
							<div class="col-sm-9"> 
								<span><?php echo $r['patient_code'];?> </span>
							</div> 
						</div> 

						<div class="form-group"> 
							<label for="inputPassword3" class="col-sm-2 control-label"><h4>Name </h4></label> 
							
							<div class="col-sm-9"> 
								<span><?php echo $r['patient_name'];?> </span>
							</div> 
						</div> 

						<div class="form-group"> 
							<label for="inputPassword3" class="col-sm-2 control-label"><h4>Birthdate</h4></label> 
							
							<div class="col-sm-9"> 
								<span><?php echo date("F d, Y",strtotime($r['patient_dob']));?> </span>
							</div> 
						</div> 
					</div>
				</div>


				<hr style="border-top: 2px solid #020202;">
				<!-- //input-forms -->

				<div class="progressbar-heading grids-heading">
						<h2>Schedule</h2>
				</div>
				<table id="table">
			<thead>
				<tr>
				<th>#</th>
				<th>Product</th>
				<th>Dosage</th>
				<th>Schedule</th>

				</tr>
			</thead>
			<tbody>
				<?php 
				$fetch =mysql_query("SELECT * from tbl_med_vacc_sched where patient_id='$id' and status=0 order by sched_date DESC");
				$count = 1;
				while ($row= mysql_fetch_array($fetch)) {

					echo" <tr>";
					echo"	<td>".$count++."</td>";
					echo"	<td>".getProdName($row['product_id'])."</td>";
					echo"	<td>".$row['dosage']."</td>";
					echo"	<td>".$row['sched_date']."<br>".date("g:i a",strtotime($row['sched_time']))."</td>";
					echo"  </tr>";
				}?>
			</tbody>
			</table>

						
				<hr style="border-top: 2px solid #020202;">
				<!-- //input-forms -->

				<div class="progressbar-heading grids-heading">
						<h2>Finish MedVac</h2>
				</div>
				<table id="table">
			<thead>
				<tr>
				<th>#</th>
				<th>Product</th>
				<th>Dosage</th>
				<th>Date Intake</th>
				<th>Nurse Note</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$fetch =mysql_query("SELECT * from tbl_med_vacc_sched where patient_id='$id' and status=1 order by date_applied DESC");
				$count = 1;
				while ($row= mysql_fetch_array($fetch)) {

					echo" <tr>";
					echo"	<td>".$count++."</td>";
					echo"	<td>".getProdName($row['product_id'])."</td>";
					echo"	<td>".$row['dosage']."</td>";
					echo"	<td>".date("M d, Y g:i a",strtotime($row['date_applied']))."</td>";
					echo"	<td>".$row['nurse_note']."</td>";
					echo"  </tr>";
				}?>
			</tbody>
			</table>

			</div>
		</div>
	</section>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/proton.js"></script>
</body>
</html>