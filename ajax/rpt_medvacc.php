<?php

include '../function.php';

$sd = $_POST['sd'];
$ed = $_POST['ed'];
?> 

<br>
<br>
<br>
<div class="row">
	<div class="col-md-12" align="center" style="font-size:14pt; font-weight:bold;">
		<h4 style="margin:0px;"> Medication and Vaccination Report </h4><h5>   <?php echo date("F d, Y",strtotime($sd))." to ".date("F d, Y",strtotime($ed)); ?> </h5>
	</div>
</div>
<table style="width: 100%;" class="table table-bordered table-striped" id="rpt_data_table">
	<thead>
		<tr>
		<th>#</th>
		<th>Patient Name</th>
		<th>Product</th>
		<th>Dosage</th>
		<th>Date Applied</th>
        <th>Assigned Nurse</th>
		<th>Remarks</th>
		<th>Nurse Note</th>
		</tr>
	</thead>
	<tbody>
		<?php 

			$end_date = date('Y-m-d H:i:s', strtotime($ed . ' +1 day'));
			$query=mysql_query("SELECT * from tbl_med_vacc_sched where status=1 and date_applied>='$sd' and date_applied<='$end_date'");
	
			
			$count=1;
			while( $row = mysql_fetch_array($query)){

		?>
		<tr >
			<td style="padding: 5px; font-size: 16px;"><?php echo $count++; ?></td>
			<td style="padding: 5px; font-size: 16px;"><?php echo getPatient($row['patient_id']); ?></td>
			<td style="padding: 5px; font-size: 16px;"><?php echo getProdName($row['product_id']); ?></td>
			<td style="padding: 5px; font-size: 16px;"><?php echo $row['dosage']; ?></td>
			<td style="padding: 5px; font-size: 16px;"><?php echo date("M d, Y",strtotime($row['date_applied'])); ?></td>
			<td style="padding: 5px; font-size: 16px;"><?php echo getGuardian($row['assign_nurse_id']); ?></td>
			<td style="padding: 5px; font-size: 16px;"><?php echo $row['remarks']; ?></td>
			<td style="padding: 5px; font-size: 16px;"><?php echo $row['nurse_note']; ?></td>

		</tr> 
	<?php }?>
	</tbody>
</table>