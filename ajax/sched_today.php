<?php 
include '../function.php';
$id=$_GET['id'];
// $id=2;

$date_today =dateToday();
?>
<!DOCTYPE html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- bootstrap-css -->
<link rel="stylesheet" href="../css/bootstrap.css">
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="../css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<link rel="stylesheet" href="../css/font.css" type="text/css"/>
<link href="../css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<script src="../js/jquery_updated.min.js"></script>
<script src="../js/modernizr.js"></script>
<script src="../js/jquery.cookie.js"></script>
<script src="../js/screenfull.js"></script>
<link rel="stylesheet" type="text/css" href="../css/table-style.css" />
<link rel="stylesheet" type="text/css" href="../css/basictable.css" />
 <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
<style type="text/css">
	#table_length,#table_info,#table_paginate	{
		display: none;
	}
</style>
</head>
<body class="dashboard-page">


	<section class="wrapper scrollable">
		
		<div>
			<div class="agile-grids">	

				<!-- input-forms -->
					<div class="progressbar-heading grids-heading">
						<h2>Schedule For Today</h2>
					</div>
					<div class="col-md-12">
						<a href="plan.php?id=<?=$id ?>" style="float: right;"><i class="fa fa-calendar"></i> View Med/Vac Plan</a>
					</div>
					
			
				<div style="text-align: center;" >
						<h5 class="animate__animated  animate__infinite" id="notif"> </h5>
				</div>
				<br><br>
					

				<!-- //input-forms -->
				<table id="table">
			<thead>
				<tr>
				<th>#</th>
				<th>Patient Name</th>
				<th>Product (Dosage)</th>
				<th>Apply</th>


				</tr>
			</thead>
			<tbody>
				<?php 
				$fetch =mysql_query("SELECT * from tbl_med_vacc_sched where assign_nurse_id='$id' and status=0 and sched_date='$date_today'");
				$count = 1;
				while ($row= mysql_fetch_array($fetch)) {

					echo" <tr>";
					echo"	<td>".$count++."</td>";
					echo"	<td>".getPatient($row['patient_id'])."</td>";
					echo"	<td>".getProdName($row['product_id'])."<br>-".$row['dosage']."</td>";
					echo"	<td><div class='bg-success pv20 text-white fw600 text-center' onclick='showModal(".$row['med_vacc_sched_id'].")'><span class='fa fa-check-circle'></span></div></td>";

					echo"  </tr>";
				}?>
			</tbody>
			</table>
			<div class="modal fade"  data-backdrop="static" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nurse Note</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<label> Nurse Note</label>
        <textarea name="nurse_note" id="nurse_note"></textarea>
        <input type="hidden" name="id" id="pateint_id">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="apply()">Save</button>
      </div>
    </div>
  </div>
</div>



			</div>
		</div>
	</section>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/proton.js"></script>
</body>
</html>
	<script type="text/javascript" src="../js/jquery.basictable.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	$('#table').dataTable({
	    aLengthMenu: [
	        [25, 50, 100, 200, -1],
	        [25, 50, 100, 200, "All"]
	    ],
	    iDisplayLength: -1
	});



});

function showModal(id){
	$("#exampleModal").modal("show")
	$("#pateint_id").val(id);
}
	
	function apply(){
		var id = $("#pateint_id").val();
		var nurse_note = $("#nurse_note").val();


	 $.ajax({
	    url:"add_apply.php",
	    method:"POST",
	     data:{
	     	id:id,
	     	nurse_note:nurse_note
	     },
	        success: function(data){
	        		$("#exampleModal").modal("hide")

	        	if(data==1){
	        		$("#notif").html('<h5 class="animate__animated  animate__tada animate__infinite alert alert-success" id="notif"> Successfully Added </h5>');
	        		setTimeout(function(){
	        		location.reload();

	        		},1000)
	        	}else if(data==2){
	        		$("#notif").html('<h5 class="animate__animated  animate__tada  alert alert-danger" id="notif"> Insufficient Quantity </h5>');
	        	}else{
	        		$("#notif").html('<h5 class="animate__animated  animate__tada  alert alert-danger" id="notif"> Error! Unable to apply medicine </h5>');
	        		// setTimeout(function(){
	        		// location.reload();

	        		// },1000)
	        	}
	      }
	    });
	}
</script>