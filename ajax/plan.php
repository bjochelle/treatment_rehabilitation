<?php 
include '../function.php';
$id=$_GET['id'];
// $id=2;

$date_today =dateToday();
?>
<!DOCTYPE html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- bootstrap-css -->
<link rel="stylesheet" href="../css/bootstrap.css">
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="../css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<link rel="stylesheet" href="../css/font.css" type="text/css"/>
<link href="../css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<script src="../js/jquery_updated.min.js"></script>
<script src="../js/modernizr.js"></script>
<script src="../js/jquery.cookie.js"></script>
<script src="../js/screenfull.js"></script>
<link rel="stylesheet" type="text/css" href="../css/table-style.css" />
<link rel="stylesheet" type="text/css" href="../css/basictable.css" />
 <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
<style type="text/css">
	#table_length,#table_info,#table_paginate	{
		display: none;
	}
</style>
</head>
<body class="dashboard-page">


	<section class="wrapper scrollable">
		
		<div>
			<div class="agile-grids">	

				<!-- input-forms -->
					<div class="progressbar-heading grids-heading">
						<h2>Schedule Plan</h2>
					</div>
					<div class="col-md-12">
						<a href="sched_today.php?id=<?=$id ?>" style="float: right;"><i class="fa fa-calendar"></i> Schedule today</a>
					</div>
					
			
				<div style="text-align: center;" >
						<h5 class="animate__animated  animate__infinite" id="notif"> </h5>
				</div>
				<br><br>
					

				<!-- //input-forms -->
				<table id="table">
			<thead>
				<tr>
				<th>#</th>
				<th>Patient Name</th>
				<th>Product >> Dosage</th>
				<th>Schedule</th>


				</tr>
			</thead>
			<tbody>
				<?php 
				$fetch =mysql_query("SELECT * from tbl_med_vacc_sched where assign_nurse_id='$id' and status=0 order by sched_date DESC");
				$count = 1;
				while ($row= mysql_fetch_array($fetch)) {

					echo" <tr>";
					echo"	<td>".$count++."</td>";
					echo"	<td>".getPatient($row['patient_id'])."</td>";
					echo"	<td>".getProdName($row['product_id'])." >><br>".$row['dosage']."</td>";
					echo"	<td>".date("M d, y <br>  g:i a",strtotime($row['sched_date']." ".$row['sched_time']))."</td>";

					echo"  </tr>";
				}?>
			</tbody>
			</table>

			</div>
		</div>
	</section>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/proton.js"></script>
</body>
</html>
	<script type="text/javascript" src="../js/jquery.basictable.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	$('#table').dataTable({
	    aLengthMenu: [
	        [25, 50, 100, 200, -1],
	        [25, 50, 100, 200, "All"]
	    ],
	    iDisplayLength: -1
	});



});
	
	function apply(id){
	 $.ajax({
	    url:"add_apply.php",
	    method:"POST",
	     data:{
	     	id:id
	     },
	        success: function(data){

	        	if(data==1){
	        		$("#notif").html('<h5 class="animate__animated  animate__tada animate__infinite alert alert-success" id="notif"> Successfully Added </h5>');
	        		setTimeout(function(){
	        		location.reload();

	        		},1000)
	        	}else if(data==2){
	        		$("#notif").html('<h5 class="animate__animated  animate__tada  alert alert-danger" id="notif"> Insufficient Quantity </h5>');
	        	}else{
	        		$("#notif").html('<h5 class="animate__animated  animate__tada  alert alert-danger" id="notif"> Error! Unable to apply medicine </h5>');
	        		// setTimeout(function(){
	        		// location.reload();

	        		// },1000)
	        	}
	      }
	    });
	}
</script>